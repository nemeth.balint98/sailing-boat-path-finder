﻿using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Logic.Interfaces
{
    // SINGLETONNAK KELL LENNIE!!!
    public interface IGeographyService
    {
        /// <summary>
        /// Returns a new BoatPosition shifted by moveVector relative to from.
        /// </summary>
        /// <param name="from">Startin position.</param>
        /// <param name="moveVector">Vector in Km.</param>
        /// <returns>BoatPosition with modified geograpchical coordinates.</returns>
        BoatPosition Move(BoatPosition from, Vector2 moveVector);

        /// <summary>
        /// Gets the vector (in Km) between two position.
        /// </summary>
        /// <param name="from">Starting point where the boat stands now.</param>
        /// <param name="to">Finish point where the boat wants to go.</param>
        /// <returns>Vector in Km.</returns>
        Vector2 GetRelativePosition(BoatPosition from, BoatPosition to);
    }
}
