﻿using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Logic.Interfaces
{
    public interface IPathCalculator
    {
        /// <summary>
        /// Calculates how much time (in seconds) to take the path from one point to other.
        /// </summary>
        /// <param name="from">Starting point where the boat stands now.</param>
        /// <param name="to">Finish point where the boat wants to go.</param>
        /// <returns>Time in seconds.</returns>
        double TimeFromTo(BoatPosition from, BoatPosition to);

        /// <summary>
        /// Sets the boat for the journey.
        /// </summary>
        /// <param name="boat">Boat to travel with.</param>
        void WithBoat(Boat boat);
    }
}
