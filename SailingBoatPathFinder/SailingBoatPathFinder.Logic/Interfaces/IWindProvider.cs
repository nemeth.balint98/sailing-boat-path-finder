﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Logic.Interfaces
{
    public interface IWindProvider
    {
        /// <summary>
        /// Gets the wind's vector (in knots) at the given time. If the wind's speed is zero at the moment, then returns the next time and wind vector when the speed is not zero.
        /// </summary>
        /// <param name="latitude">Geographycal latitude.</param>
        /// <param name="longitude">Geographycal longitude.</param>
        /// <param name="when">The moment of time when the wind needed</param>
        /// <returns>Vector of wind.</returns>
        public Vector2 GetWindAt(double latitude, double longitude, out DateTime when);

        /// <summary>
        /// Preloads an area wind data. Should be called between each checkpoints.
        /// </summary>
        /// <param name="latitude1">Latitude of one corner of area.</param>
        /// <param name="longitude1">Longitude of one corner of area.</param>
        /// <param name="latitude2">Latitude of other corner of area.</param>
        /// <param name="longitude2">Longitude of other corner of area.</param>
        /// <param name="when">The time when the wind data needed.</param>
        public void PreloadArea(double latitude1, double longitude1, double latitude2, double longitude2, DateTime when);
    }
}
