﻿using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Logic.Interfaces
{
    public interface IDirectionService
    {
        /// <summary>
        /// Gets the possible next steps.
        /// </summary>
        /// <param name="boatPosition">Where the boat stands now.</param>
        /// <returns>Enumerable of new positions.</returns>
        IEnumerable<BoatPosition> GetNeighbours(BoatPosition boatPosition);
    }
}
