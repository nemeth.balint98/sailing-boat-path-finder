﻿using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Logic.Interfaces
{
    public interface IPathFinder
    {
        IPathFinder WithBoat(Boat boat);

        IPathFinder From(double latitude, double longitude);

        IPathFinder To(double latitude, double longitude);

        IEnumerable<BoatPosition> Start();
    }
}
