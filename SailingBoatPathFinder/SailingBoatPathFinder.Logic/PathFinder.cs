﻿using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Logic.Interfaces;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Logic
{
    public class PathFinder : IPathFinder
    {
        private readonly IDirectionService _directionService;
        private readonly IPathCalculator _pathCalculator;
        private readonly IGeographyService _geographyService;
        private BoatPosition _from;
        private BoatPosition _to;
        private BoatPosition topLeft;
        private BoatPosition bottomRight;

        public PathFinder(IDirectionService directionService, IPathCalculator pathCalculator, IGeographyService geographyService)
        {
            _directionService = directionService;
            _pathCalculator = pathCalculator;
            _geographyService = geographyService;
        }

        public IPathFinder From(double latitude, double longitude)
        {
            _from = new BoatPosition(latitude, longitude);
            return this;
        }

        public IPathFinder To(double latitude, double longitude)
        {
            _to = new BoatPosition(latitude, longitude);
            topLeft = _geographyService.Move(_to, new Vector2(-(float)PathFinderConfig.StepDistance * 1.1f, (float)PathFinderConfig.StepDistance * 1.1f));
            bottomRight = _geographyService.Move(_to, new Vector2((float)PathFinderConfig.StepDistance * 1.1f, -(float)PathFinderConfig.StepDistance * 1.1f));
            return this;
        }

        public IPathFinder WithBoat(Boat boat)
        {
            _pathCalculator.WithBoat(boat);
            return this;
        }
        
        public IEnumerable<BoatPosition> Start()
        {
            FindPath();
            var result = ResolvePath();
            return result;
        }

        private void FindPath()
        {
            var Q = new List<BoatPosition>() { _from };
            var dist = new Dictionary<BoatPosition, double> { { _from, 0 } };
            var found = false;

            while (Q.Any() && !found)
            {
                var u = Q.OrderBy(x => dist[x]).First();
                Q.Remove(u);

                var neighbors = _directionService.GetNeighbours(u);
                Q.AddRange(neighbors);

                foreach (BoatPosition neighbor in neighbors)
                {
                    var time = _pathCalculator.TimeFromTo(u, neighbor);
                    var alt_u = dist[u] + time;

                    if (!dist.TryGetValue(neighbor, out double alt_neighbor) || alt_u < alt_neighbor)
                    {
                        // new neighbor or quicker path found
                        dist[neighbor] = alt_u;
                        neighbor.From = u;
                        neighbor.Seconds = time;
                        found = IsFound(neighbor);
                    }
                }
            }
        }

        private IEnumerable<BoatPosition> ResolvePath()
        {
            var current = _to;
            while(current != null)
            {
                yield return current;
                current = current.From;
            }
        }

        private bool IsFound(BoatPosition position)
        {
            if (position.Latitude <= topLeft.Latitude
                && position.Latitude >= bottomRight.Latitude
                && position.Longitude >= topLeft.Longitude
                && position.Longitude <= bottomRight.Longitude)
            {
                _to = position;
                return true;
            }

            return false;
        }
    }
}
