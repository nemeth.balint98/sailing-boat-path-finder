﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Logic.Models
{
    public class BoatPosition
    {
        public BoatPosition(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }

        public BoatPosition From { get; set; }

        public double Seconds { get; set; }

        public double Longitude { get; set; }

        public double Latitude { get; set; }
    }
}
