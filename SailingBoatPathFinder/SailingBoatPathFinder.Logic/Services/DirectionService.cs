﻿using SailingBoatPathFinder.Logic.Interfaces;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Logic.Services
{
    public class DirectionService : IDirectionService
    {
        private readonly IGeographyService _geographyService;
        private readonly Dictionary<(int, int), Vector2> vectorCache;

        public DirectionService(IGeographyService geographyService)
        {
            vectorCache = new Dictionary<(int,int), Vector2>();
            _geographyService = geographyService;
            FillCache();
        }

        /// <summary>
        /// Pre-generates the direction vectors that a boat can move from a position.
        /// </summary>
        private void FillCache()
        {
            int x = -PathFinderConfig.DirectionResolution;
            int y = PathFinderConfig.DirectionResolution;
            
            for (int i = 0; i < PathFinderConfig.DirectionResolution * 2; i++)
            {
                vectorCache[(x,y)] = new Vector2(x, y);
                x++;
            }
            x--;

            for (int i = 0; i < PathFinderConfig.DirectionResolution * 2; i++)
            {
                vectorCache[(x, y)] = new Vector2(x, y);
                y--;
            }
            y++;

            for (int i = 0; i < PathFinderConfig.DirectionResolution * 2; i++)
            {
                vectorCache[(x, y)] = new Vector2(x, y);
                x--;
            }
            x++;

            for (int i = 0; i < PathFinderConfig.DirectionResolution * 2; i++)
            {
                vectorCache[(x, y)] = new Vector2(x, y);
                y++;
            }
        }

       /// <inheritdoc/>
        public IEnumerable<BoatPosition> GetNeighbours(BoatPosition boatPosition)
        {
            return vectorCache.Select(x => _geographyService.Move(boatPosition, x.Value));
        }
    }
}
