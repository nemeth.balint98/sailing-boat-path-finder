﻿using SailingBoatPathFinder.Logic.Interfaces;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Logic.Services
{
    // SINGLETONNAK KELL LENNIE!!!
    public class GeographyService : IGeographyService
    {
        private const int EARTH_RADIUS = 6378;
        private readonly List<BoatPosition> boatPositionCache;
        private readonly Dictionary<(BoatPosition, BoatPosition), Vector2> moveCache;

        public GeographyService()
        {
            boatPositionCache = new List<BoatPosition>();
            moveCache = new Dictionary<(BoatPosition, BoatPosition), Vector2>();
        }

        /// <inheritdoc/>
        public BoatPosition Move(BoatPosition from, Vector2 moveVector)
        {
            // https://stackoverflow.com/questions/7477003/calculating-new-longitude-latitude-from-old-n-meters
            var new_latitude = from.Latitude + (moveVector.Y * PathFinderConfig.StepDistance / EARTH_RADIUS) * (180 / Math.PI);
            var new_longitude = from.Longitude + (moveVector.X * PathFinderConfig.StepDistance / EARTH_RADIUS) * (180 / Math.PI) / Math.Cos(from.Latitude * Math.PI / 180);

            // TODO: megnézni h használja e a cache-t
            var newPosition = boatPositionCache.FirstOrDefault(x => x.Latitude == new_latitude && x.Longitude == new_longitude);
            if(newPosition == null)
            {
                newPosition = new BoatPosition(new_latitude, new_longitude);
                boatPositionCache.Add(newPosition);
            }

            moveCache[(from, newPosition)] = moveVector;

            return newPosition;
        }

        /// <inheritdoc/>
        public Vector2 GetRelativePosition(BoatPosition from, BoatPosition to)
        {
            return moveCache[(from, to)];
        }
    }
}
