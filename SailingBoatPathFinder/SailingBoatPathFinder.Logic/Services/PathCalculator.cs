﻿using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Data.Interfaces;
using SailingBoatPathFinder.Logic.Interfaces;
using SailingBoatPathFinder.Logic.Models;
using System;
using System.Linq;
using System.Numerics;

namespace SailingBoatPathFinder.Logic.Services
{
    public class PathCalculator : IPathCalculator
    {
        private readonly IGeographyService _geographyService;
        private readonly IWindProvider _windProvider;
        private Boat _boat;

        public PathCalculator(IGeographyService geographyService, IWindProvider windProvider)
        {
            _geographyService = geographyService;
            _windProvider = windProvider;
        }

        /// <inheritdoc/>
        public void WithBoat(Boat boat)
        {
            _boat = boat;
        }

        /// <inheritdoc/>
        public double TimeFromTo(BoatPosition from, BoatPosition to)
        {
            var directionVector = _geographyService.GetRelativePosition(from, to);
            var timeOfWind = DateTime.Now;
            var prevTimeOfWind = timeOfWind;
            var windVector = _windProvider.GetWindAt(from.Latitude, from.Longitude, out timeOfWind);

            var degreesRelativeToBoat = DegreesBetween(directionVector, windVector);
            var speed = CalculateSpeed(degreesRelativeToBoat, windVector.Length());

            var hours = directionVector.Length() / (speed * 1.852);

            return hours * 60 * 60 + (timeOfWind - prevTimeOfWind).TotalSeconds;
        }

        // TODO: cache-elni
        private double DegreesBetween(Vector2 v1, Vector2 v2)
        {
            var degrees = Math.Acos((v1.X * v2.X + v1.Y * v2.Y) / (v1.Length() * v2.Length())) * 180 / Math.PI;
            return 180 - degrees;
        }

        private double CalculateSpeed(double windAngle, float windSpeed)
        {
            var beatAngle = _boat.BeatAngleBy(windSpeed);
            var runAngle = _boat.RunAngleBy(windSpeed);
            if (windAngle < beatAngle || windAngle > runAngle) return 0;

            var (topLeft, topRight, bottomRight, bottomLeft) = GetFourCorner(windAngle, windSpeed);
            var (topLeftWeight, topRightWeight, bottomRightWeight, bottomLeftWeight) = CalculateFourWeights(windAngle, windSpeed, topLeft, topRight, bottomLeft);

            var speed = topLeft.GetBoatVelocity() * topLeftWeight
                + topRight.GetBoatVelocity() * topRightWeight
                + bottomRight.GetBoatVelocity() * bottomRightWeight
                + bottomLeft.GetBoatVelocity() * bottomLeftWeight;

            return speed;
        }

        /// <summary>
        /// Gets the 4 discrete values of the polar diagram by the intermediete wind angle and wind speed.
        /// </summary>
        /// <param name="windAngle">Angle of wind relative to boat.</param>
        /// <param name="windSpeed">Speed of wind.</param>
        /// <returns>Tuple of 4 corner polar data.</returns>
        private (IPolarData, IPolarData, IPolarData, IPolarData) GetFourCorner(double windAngle, float windSpeed)
        {
            // upper and lower row of the polar diagram table
            var lowerPolarDatasByAngle = _boat.PolarDatas.Where(x => x.Angle <= windAngle).OrderByDescending(x => x.Angle);
            var greaterPolarDatasByAngle = _boat.PolarDatas.Where(x => x.Angle >= windAngle).OrderBy(x => x.Angle);

            IPolarData topLeft = null;
            IPolarData topRight = null;
            IPolarData bottomLeft = null;
            IPolarData bottomRight = null;

            // if wind angle lower than we have information about (52 degrees) -> using beat values
            if (!lowerPolarDatasByAngle.Any())
            {
                (topLeft, topRight) = GetExtremeAngleValues(windAngle, windSpeed);
                topLeft.UseBeatAngle();
                topRight.UseBeatAngle();
            }

            // if wind angle greater than we have information about (150 degrees) -> using run values
            if (!greaterPolarDatasByAngle.Any())
            {
                (bottomLeft, bottomRight) = GetExtremeAngleValues(windAngle, windSpeed);
                bottomLeft.UseRunAngle();
                bottomRight.UseRunAngle();
            }

            // left and right column of the rows of polar diagram table
            topLeft ??= lowerPolarDatasByAngle.Where(x => x.WindVelocity <= windSpeed).OrderByDescending(x => x.WindVelocity).FirstOrDefault();
            topRight ??= lowerPolarDatasByAngle.Where(x => x.WindVelocity >= windSpeed).OrderBy(x => x.WindVelocity).FirstOrDefault();
            bottomLeft ??= greaterPolarDatasByAngle.Where(x => x.WindVelocity <= windSpeed).OrderByDescending(x => x.WindVelocity).FirstOrDefault();
            bottomRight ??= greaterPolarDatasByAngle.Where(x => x.WindVelocity >= windSpeed).OrderBy(x => x.WindVelocity).FirstOrDefault();

            // if wind speed is greater or lower than we have information about (6-20kts)
            topLeft ??= PolarData.GetDefault(topRight.GetWindVelocity());
            bottomLeft ??= PolarData.GetDefault(bottomRight.GetWindVelocity());
            topRight ??= topLeft;
            bottomRight ??= bottomLeft;

            return (topLeft, topRight, bottomRight, bottomLeft);
        }

        /// <summary>
        /// Calculates what weights the 4 discrete values should be taken with.
        /// </summary>
        /// <param name="windAngle">Angle of wind relative to boat.</param>
        /// <param name="windSpeed">Speed of wind.</param>
        /// <param name="topLeft">Lower wind speed, lower angle from the polar diagram.</param>
        /// <param name="topRight">Greater wind speed, lower angle from the polar diagram.</param>
        /// <param name="bottomLeft">Lower wind speed, Greater angle from the polar diagram.</param>
        /// <returns>Tuple of four weights.</returns>
        private (double, double, double, double) CalculateFourWeights(double windAngle, float windSpeed, IPolarData topLeft, IPolarData topRight, IPolarData bottomLeft)
        {
            // A is horizontal side
            // B is vertical side
            var topLeftWeightA = (windSpeed - topLeft.GetWindVelocity()) / (topRight.GetWindVelocity() - topLeft.GetWindVelocity());
            var topLeftWeightB = (windAngle - topLeft.GetWindAngle()) / (topRight.GetWindAngle() - bottomLeft.GetWindAngle());
            var topLeftWeight = topLeftWeightA * topLeftWeightB;

            var topRightWeightA = 1 - topLeftWeightA;
            var topRightWeightB = topLeftWeightB;
            var topRightWeight = topRightWeightA * topRightWeightB;

            var bottomRightWeightA = topRightWeightA;
            var bottomRightWeightB = 1 - topLeftWeightB;
            var bottomRightWeight = bottomRightWeightA * bottomRightWeightB;

            var bottomLeftWeightA = topLeftWeightA;
            var bottomLeftWeightB = bottomRightWeightB;
            var bottomLeftWeight = bottomLeftWeightA * bottomLeftWeightB;

            return (topLeftWeight, topRightWeight, bottomRightWeight, bottomLeftWeight);
        }

        /// <summary>
        /// Gets polar datas which angles are near beat or run.
        /// </summary>
        /// <param name="windAngle">Angle of wind relative to boat.</param>
        /// <param name="windSpeed">Speed of wind.</param>
        /// <returns>Tuple of two polar datas.</returns>
        private (IPolarData, IPolarData) GetExtremeAngleValues(double windAngle, float windSpeed)
        {
            var lower = _boat.Properties.Where(x => x.WindVelocity <= windSpeed).OrderByDescending(x => x.WindVelocity).FirstOrDefault();
            var greater = _boat.Properties.Where(x => x.WindVelocity >= windSpeed).OrderBy(x => x.WindVelocity).FirstOrDefault();

            var left = lower ?? PolarData.GetDefault(windAngle);
            var right = greater ?? lower;

            return (left, right);
        }
    }
}
