﻿using SailingBoatPathFinder.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Logic
{
    public class WindProvider : IWindProvider
    {
        private static Random _rand;

        public WindProvider()
        {
            _rand = new Random();
        }

        /// <inheritdoc/>
        public Vector2 GetWindAt(double latitude, double longitude, out DateTime when)
        {
            when = DateTime.Now;
            // maximum wind speed is 21.21 knots
            return new Vector2(_rand.Next(-150, 150) / 10, _rand.Next(-150, 150) / 10);
        }

        public void PreloadArea(double latitude1, double longitude1, double latitude2, double longitude2, DateTime when)
        {
            throw new NotImplementedException();
        }
    }
}
