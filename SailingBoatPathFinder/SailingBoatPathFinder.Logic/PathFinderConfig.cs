﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Logic
{
    public static class PathFinderConfig
    {
        public static double StepDistance = 0.1; // 1 step in direction of E,N,W,S = DirectionResolution * StepDistance in Km
        public static int DirectionResolution = 4; // number of directions from a point -> 8 * value
    }
}
