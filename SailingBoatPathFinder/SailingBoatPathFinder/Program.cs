﻿using Microsoft.EntityFrameworkCore;
using SailingBoatPathFinder.Communication.Services;
using SailingBoatPathFinder.Data;
using SailingBoatPathFinder.Logic;
using SailingBoatPathFinder.Logic.Models;
using SailingBoatPathFinder.Logic.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SailingBoatPathFinder
{
    class Program
    {
        static void Main()
        {
            var boatDownloaderService = new BoatDataDownloaderService();
            var context = new ApplicationDbContext();
            var seeder = new DatabaseSeeder(boatDownloaderService, context);

            Console.WriteLine("Downloading boat datas into database if seeding necessary...");
            seeder.Seed().Wait();
            Console.WriteLine("\nSeeding completed");

            var pathFinder = new PathFinder(null, null, null);
            var boat = context.Boat.First();

            //IEnumerable<BoatPosition> result = pathFinder.From(47.052429, 18.038576)
            //    .To(47.025956, 18.061292)
            //    .WithBoat(boat)
            //    .Start();

            Console.ReadLine();
        }
    }
}
