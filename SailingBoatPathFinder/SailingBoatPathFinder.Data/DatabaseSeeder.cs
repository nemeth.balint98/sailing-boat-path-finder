﻿using Microsoft.EntityFrameworkCore;
using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Data
{
    public class DatabaseSeeder
    {
        private readonly IBoatDataDownloaderService _boatDatadownloader;
        private readonly ApplicationDbContext _context;

        public DatabaseSeeder(IBoatDataDownloaderService boatDatadownloader, ApplicationDbContext context)
        {
            _boatDatadownloader = boatDatadownloader;
            _context = context;
        }

        public async Task Seed()
        {
            _context.Database.Migrate();

            if (!await _context.Boat.AnyAsync())
            {
                await _context.Boat.AddRangeAsync(await _boatDatadownloader.DownloadDatasAsync());
                _context.SaveChanges();
            }
        }
    }
}
