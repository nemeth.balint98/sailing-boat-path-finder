﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SailingBoatPathFinder.Data.Migrations
{
    public partial class Create_Database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Boat",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boat", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BoatProperty",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    WindVelocity = table.Column<int>(type: "int", nullable: false),
                    BeatAngle = table.Column<double>(type: "float", nullable: false),
                    BeatVMG = table.Column<double>(type: "float", nullable: false),
                    RunVMG = table.Column<double>(type: "float", nullable: false),
                    RunAngle = table.Column<double>(type: "float", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoatProperty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PolarData",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Angle = table.Column<double>(type: "float", nullable: false),
                    WindVelocity = table.Column<double>(type: "float", nullable: false),
                    BoatVelocity = table.Column<double>(type: "float", nullable: false),
                    OwnerBoatId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PolarData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PolarData_Boat_OwnerBoatId",
                        column: x => x.OwnerBoatId,
                        principalTable: "Boat",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BoatBoatProperty",
                columns: table => new
                {
                    OwnerBoatId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PropertiesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoatBoatProperty", x => new { x.OwnerBoatId, x.PropertiesId });
                    table.ForeignKey(
                        name: "FK_BoatBoatProperty_Boat_OwnerBoatId",
                        column: x => x.OwnerBoatId,
                        principalTable: "Boat",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BoatBoatProperty_BoatProperty_PropertiesId",
                        column: x => x.PropertiesId,
                        principalTable: "BoatProperty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BoatBoatProperty_PropertiesId",
                table: "BoatBoatProperty",
                column: "PropertiesId");

            migrationBuilder.CreateIndex(
                name: "IX_PolarData_OwnerBoatId",
                table: "PolarData",
                column: "OwnerBoatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BoatBoatProperty");

            migrationBuilder.DropTable(
                name: "PolarData");

            migrationBuilder.DropTable(
                name: "BoatProperty");

            migrationBuilder.DropTable(
                name: "Boat");
        }
    }
}
