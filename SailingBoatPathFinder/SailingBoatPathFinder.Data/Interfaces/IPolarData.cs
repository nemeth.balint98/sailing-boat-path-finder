﻿using SailingBoatPathFinder.Data.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Data.Interfaces
{
    public interface IPolarData
    {
        double GetWindVelocity();

        double GetWindAngle();

        double GetBoatVelocity();

        void UseBeatAngle();

        void UseRunAngle();
    }
}
