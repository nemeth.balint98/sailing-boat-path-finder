﻿using SailingBoatPathFinder.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Data.Interfaces
{
    public interface IBoatDataDownloaderService
    {
        Task<IEnumerable<Boat>> DownloadDatasAsync();
    }
}
