﻿using Microsoft.EntityFrameworkCore;
using SailingBoatPathFinder.Data.Entities;
using System;
using System.IO;

namespace SailingBoatPathFinder.Data
{
    public class ApplicationDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbPath = AppDomain.CurrentDomain.BaseDirectory + "Database.mdf";
            optionsBuilder.UseSqlServer(@$"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename={dbPath};Integrated Security=True");
        }

        public DbSet<Boat> Boat { get; set; }

        public DbSet<PolarData> PolarData { get; set; }

        public DbSet<BoatProperty> BoatProperty { get; set; }
    }
}
