﻿using Newtonsoft.Json;
using SailingBoatPathFinder.Data.Enums;
using SailingBoatPathFinder.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Data.Entities
{
    public class BoatProperty : IPolarData
    {
        private EWindDirectionType? type = null;

        [JsonIgnore]
        public Guid Id { get; set; }

        public int WindVelocity { get; set; }

        public double BeatAngle { get; set; }

        public double BeatVMG { get; set; }

        public double RunVMG { get; set; }

        public double RunAngle { get; set; }

        [JsonIgnore]
        public IEnumerable<Boat> OwnerBoat { get; set; }

        public double GetBoatVelocity()
        {
            if (type == null)
                throw new ArgumentException(nameof(type));

            double boatVelocity;
            if (type == EWindDirectionType.Beat)
            {
                boatVelocity = BeatVMG;
            }
            else // Run
            {
                boatVelocity = RunVMG;
            }

            boatVelocity /= Math.Cos(GetWindAngle());

            return boatVelocity;
        }

        public double GetWindAngle()
        {
            if (type == null)
                throw new ArgumentException(nameof(type));

            if (type == EWindDirectionType.Beat)
            {
                return BeatAngle;
            }
            else // Run
            {
                return RunAngle;
            }
        }

        public double GetWindVelocity()
        {
            return WindVelocity;
        }

        public void UseRunAngle()
        {
            type = EWindDirectionType.Run;
        }

        public void UseBeatAngle()
        {
            type = EWindDirectionType.Beat;
        }
    }
}
