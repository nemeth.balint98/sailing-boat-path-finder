﻿using Newtonsoft.Json;
using SailingBoatPathFinder.Data.Enums;
using SailingBoatPathFinder.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Data.Entities
{
    public class PolarData : IPolarData
    {
        private static readonly PolarData @default = new PolarData() { BoatVelocity = 0, WindVelocity = 0 };

        [JsonIgnore]
        public Guid Id { get; set; }

        public double Angle { get; set; }

        public double WindVelocity { get; set; }

        public double BoatVelocity { get; set; }

        [JsonIgnore]
        public Boat OwnerBoat { get; set; }

        /// <summary>
        /// Creates or returns a cached default instance of IPolarData. Used for not to call ctor because of code speed.
        /// </summary>
        /// <param name="windAngle">Angle of wind relative to boat.</param>
        /// <returns>Default IPolarData with updated angle.</returns>
        public static IPolarData GetDefault(double windAngle)
        {
            @default.Angle = windAngle;
            return @default;
        }

        public double GetBoatVelocity()
        {
            return BoatVelocity;
        }

        public double GetWindAngle()
        {
            return Angle;
        }

        public double GetWindVelocity()
        {
            return WindVelocity;
        }

        public void UseBeatAngle()
        {
            // nothing has to happen
        }

        public void UseRunAngle()
        {
            // nothing has to happen
        }
    }
}
