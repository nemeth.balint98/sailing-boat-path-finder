﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SailingBoatPathFinder.Data.Entities
{
    public class Boat
    {
        [JsonIgnore]
        public Guid Id { get; set; }

        public string Type { get; set; }

        public ICollection<PolarData> PolarDatas { get; set; }

        public ICollection<BoatProperty> Properties { get; set; }

        public double BeatAngleBy(float windSpeed)
        {
            var min = Properties.OrderBy(x => x.WindVelocity).First();
            var max = Properties.OrderBy(x => x.WindVelocity).Last();

            var lower = Properties.Where(x => x.WindVelocity <= windSpeed).OrderByDescending(x => x.WindVelocity).FirstOrDefault() ?? min;
            var greater = Properties.Where(x => x.WindVelocity >= windSpeed).OrderBy(x => x.WindVelocity).FirstOrDefault() ?? max;

            var angleDifference = greater.BeatAngle - lower.BeatAngle;
            var ratio = (greater.WindVelocity - windSpeed) / (greater.WindVelocity - greater.WindVelocity);

            var angle = lower.BeatAngle + angleDifference * ratio;
            return angle;
        }

        public double RunAngleBy(float windSpeed)
        {
            var min = Properties.OrderBy(x => x.WindVelocity).First();
            var max = Properties.OrderBy(x => x.WindVelocity).Last();

            var lower = Properties.Where(x => x.WindVelocity <= windSpeed).OrderByDescending(x => x.WindVelocity).FirstOrDefault() ?? min;
            var greater = Properties.Where(x => x.WindVelocity >= windSpeed).OrderBy(x => x.WindVelocity).FirstOrDefault() ?? max;

            var angleDifference = greater.RunAngle - lower.RunAngle;
            var ratio = (greater.WindVelocity - windSpeed) / (greater.WindVelocity - greater.WindVelocity);

            var angle = lower.RunAngle + angleDifference * ratio;
            return angle;
        }
    }
}
