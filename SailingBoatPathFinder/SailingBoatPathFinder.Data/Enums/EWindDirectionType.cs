﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Data.Enums
{
    public enum EWindDirectionType
    {
        Beat = 1,
        Run = 2
    }
}
