using NUnit.Framework;
using SailingBoatPathFinder.Communication.Services;
using System;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Communication.Test
{
    public class WindDataDownloaderTest
    {
        private WindDataDownloader windDataDownloader;

        [SetUp]
        public void Setup()
        {
            this.windDataDownloader = new WindDataDownloader();
        }


        [TestCase(5)]
        [TestCase(23)]
        public void DownloadAsync_should_throwIllegalArgumentException_when_invalidTimeGiven(int hour)
        {
            var time = new DateTime(2021, 10, 31, hour, 0, 0);
            Assert.ThrowsAsync<ArgumentException>(() => this.windDataDownloader.DownloadAsync(0, 0, 0, 0, time));
        }
    }
}