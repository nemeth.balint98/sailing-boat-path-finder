﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace SailingBoatPathFinder.Communication.Models
{
    public class WindData
    {
        public WindHeader Header { get; set; }

        public Vector2[,] WindSpeeds { get; set; }

        public WindData(WindHeader header, Vector2[,] windSpeeds)
        {
            Header = header;
            WindSpeeds = windSpeeds;
        }
    }
}
