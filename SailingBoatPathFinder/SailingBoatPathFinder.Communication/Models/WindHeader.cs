﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SailingBoatPathFinder.Communication.Models
{
    public class WindHeader
    {
        public double Latitude1 { get; set; }

        public double Longitude1 { get; set; }
        
        public double Latitude2 { get; set; }
        
        public double Longitude2 { get; set; }

        public double DX { get; set; }

        public double DY { get; set; }

        public int NX { get; set; }

        public int NY { get; set; }

        public DateTime Time { get; set; }

        public WindHeader(double latitude1, double longitude1, double latitude2, double longitude2, double dX, double dY, int nx, int ny, DateTime time)
        {
            Latitude1 = latitude1;
            Longitude1 = longitude1;
            Latitude2 = latitude2;
            Longitude2 = longitude2;
            DX = dX;
            DY = dY;
            NX = nx;
            NY = ny;
            Time = time;
        }
    }
}
