﻿using Newtonsoft.Json;
using SailingBoatPathFinder.Communication.Interfaces;
using SailingBoatPathFinder.Communication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Communication.Services
{
    public class WindDataDownloader : IWindDataDownloader
    {
        private const string URL = "https://weather4sport.com/Services/userService.asmx/GetWeatherAnimationData";
        private readonly HttpClient client;

        public WindDataDownloader()
        {
            this.client = new HttpClient();
        }

        public async Task<WindData> DownloadAsync(double latitude1, double longitude1, double latitude2, double longitude2, DateTime whenUTC)
        {
            if (whenUTC.Hour % 3 != 0 || whenUTC.Hour < 0 || whenUTC.Hour > 21)
                throw new ArgumentException("WhenUTC's hour must be divisible by 3 and between 0 and 21.");

            var response = await this.client.PostAsync(URL, this.CreateRequestBody(latitude1, longitude1, latitude2, longitude2, whenUTC));
            var content = await response.Content.ReadAsStringAsync();

            return CreateWindData(content, whenUTC);
        }

        private HttpContent CreateRequestBody(double latitude1, double longitude1, double latitude2, double longitude2, DateTime whenUTC)
        {
            return new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "lat0", latitude1.ToString() },
                { "lat1", latitude2.ToString() },
                { "lon0", longitude1.ToString() },
                { "lon1", longitude2.ToString() },
                { "type", "wind" },
                { "datetime", whenUTC.ToString("yyyy-MM-ddThh:mm:ssZ") },
                { "zoom", "3" },
            });
        }

        private WindData CreateWindData(string jsonString, DateTime when)
        {
            var root = JsonConvert.DeserializeXNode($"{{root: {jsonString} }}").Root;
            var headerElement = root.Element("d")
                .Element("weather_anim_data")
                .Elements()
                .First()
                .Element("header");

            var header = new WindHeader(
                double.Parse(headerElement.Element("la0").Value),
                double.Parse(headerElement.Element("lo0").Value),
                double.Parse(headerElement.Element("la1").Value),
                double.Parse(headerElement.Element("lo1").Value),
                double.Parse(headerElement.Element("dx").Value),
                double.Parse(headerElement.Element("dy").Value),
                int.Parse(headerElement.Element("nx").Value),
                int.Parse(headerElement.Element("ny").Value),
                when);

            var windSpeedsX = root.Element("d")
                .Element("weather_anim_data")
                .Elements()
                .First()
                .Element("data")
                .Elements()
                .Select(x => Convert.ToDouble(x.Value))
                .ToArray();

            var windSpeedsY = root.Element("d")
                .Element("weather_anim_data")
                .Elements()
                .Last()
                .Element("data")
                .Elements()
                .Select(x => Convert.ToDouble(x.Value))
                .ToArray();

            var vectors = new Vector2[header.NX, header.NY];
            for (int i = 0; i < vectors.GetLength(0); i++)
            {
                for (int j = 0; j < vectors.GetLength(1); j++)
                {
                    var index = i * header.NX + j;
                    vectors[i, j] = new Vector2((float)windSpeedsX[index], (float)windSpeedsY[index]);
                }
            }

            return new WindData(header, vectors);
        }
    }
}
