﻿using SailingBoatPathFinder.Communication.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.Communication.Interfaces
{
    public interface IWindDataDownloader
    {
        /// <summary>
        /// Sends a HTTP request to get wind data of the given area.
        /// </summary>
        /// <param name="latitude1">Latitude of one corner of the area.</param>
        /// <param name="longitude1">Longitude of one corner of the area.</param>
        /// <param name="latitude2">Latitude of the other corner of the area.</param>
        /// <param name="longitude2">Longitude of the other corner of the area.</param>
        /// <param name="whenUTC">Time when the data is needed in UTC.</param>
        /// <returns>Deserialized wind data.</returns>
        Task<WindData> DownloadAsync(double latitude1, double longitude1, double latitude2, double longitude2, DateTime whenUTC);
    }
}
