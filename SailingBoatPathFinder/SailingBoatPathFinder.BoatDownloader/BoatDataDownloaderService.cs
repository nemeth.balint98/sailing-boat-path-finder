﻿using Newtonsoft.Json;
using SailingBoatPathFinder.Data.Entities;
using SailingBoatPathFinder.Data.Extensions;
using SailingBoatPathFinder.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SailingBoatPathFinder.Communication.Services
{
    public class BoatDataDownloaderService
    {
        private static readonly Uri URL = new Uri("https://jieter.github.io/orc-data/site/");

        private readonly string[] windAngles = new string[] { "52", "60", "75", "90", "110", "120", "135", "150" };
        private readonly int[] windVelocities = new int[] { 6, 8, 10, 12, 14, 16, 20 };
        private int allBoatsCount = 0;
        private int downloadedBoatsCount = 0;

        public async Task<IEnumerable<Boat>> DownloadDatasAsync()
        {
            var jsonString = new WebClient().DownloadString(URL + "index.json");
            var doc = JsonConvert.DeserializeXNode($"{{ root: {jsonString} }}", "root");
            var boats = doc.Root.Elements().DistinctBy(x => x.Elements().Skip(2).First().Value);
            allBoatsCount = boats.Count();

            var tasks = boats.Select(boat => Task.Factory.StartNew(state =>
            {
                var sailNumber = (state as XElement).Elements().First().Value;
                var result = GetBoat(sailNumber);

                Interlocked.Increment(ref downloadedBoatsCount);
                Console.Write($"\r{downloadedBoatsCount} / {allBoatsCount} downloaded");

                return result;
            }, boat, TaskCreationOptions.LongRunning));

            var downloadedBoats = await Task.WhenAll(tasks);

            return downloadedBoats.Where(x => x != null);
        }

        private Boat GetBoat(string fileName)
        {
            string jsonString;
            try
            {
                jsonString = new WebClient().DownloadString($"{URL}/data/{fileName}.json");
            }
            catch (WebException)
            {
                return null;
            }

            jsonString = ReplaceNumberNodesWithStringNodes(jsonString);
            var root = JsonConvert.DeserializeXNode($"{{root: {jsonString} }}").Root;

            return new Boat()
            {
                PolarDatas = GetPolarDatas(root).ToList(),
                Properties = GetBeatAngles(root).ToList(),
                Type = root.Element("boat").Element("type").Value
            };
        }

        private IEnumerable<BoatProperty> GetBeatAngles(XElement root)
        {
            var angles = root.Element("vpp").Elements("beat_angle").ToList();
            var runVMGs = root.Element("vpp").Elements("run_vmg").ToList();
            var beatVMGs = root.Element("vpp").Elements("beat_vmg").ToList();
            var runAngles = root.Element("vpp").Elements("run_angle").ToList();

            for (int i = 0; i < windVelocities.Length; i++)
            {
                yield return new BoatProperty()
                {
                    BeatAngle = double.Parse(angles[i].Value.Replace('.', ',')),
                    BeatVMG = double.Parse(beatVMGs[i].Value.Replace('.', ',')),
                    RunVMG = double.Parse(runVMGs[i].Value.Replace('.', ',')),
                    WindVelocity = windVelocities[i],
                    RunAngle = double.Parse(runAngles[i].Value.Replace('.', ',')),
                };
            }
        }

        private IEnumerable<PolarData> GetPolarDatas(XElement root)
        {
            for (int i = 0; i < windAngles.Length; i++)
            {
                var windAngle = int.Parse(windAngles[i]);
                var boatVelocities = root.Element("vpp").Elements($"d{windAngle}");
                int j = 0;

                foreach (var boatVelocity in boatVelocities)
                {
                    yield return new PolarData()
                    {
                        Angle = windAngle,
                        BoatVelocity = double.Parse(boatVelocity.Value.Replace('.', ',')),
                        WindVelocity = windVelocities[j++]
                    };
                }
            }
        }

        private string ReplaceNumberNodesWithStringNodes(string jsonString)
        {
            foreach (var angle in windAngles)
            {
                jsonString = jsonString.Replace($"\"{angle}\":", $"\"d{angle}\":");
            }
            return jsonString;
        }
    }
}
