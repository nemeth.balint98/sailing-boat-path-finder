﻿using Newtonsoft.Json;
using SailingBoatPathFinder.Communication.Services;
using SailingBoatPathFinder.Data.Entities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SailingBoatPathFinder.BoatDownloader
{
    class Program
    {
        static async Task Main()
        {
            //var json = await File.ReadAllTextAsync("C:\\Users\\nemet\\source\\repos\\OE\\Projectmunka\\sailing-boat-path-finder\\SailingBoatPathFinder\\SailingBoatPathFinder.BoatDownloader\\bin\\Debug\\netcoreapp3.1\\boats.json");
            //var objects = JsonConvert.DeserializeObject<List<Boat>>(json);

            var downloader = new BoatDataDownloaderService();
            var boats = await downloader.DownloadDatasAsync();
            var boatsJson = JsonConvert.SerializeObject(boats);
            await File.WriteAllTextAsync("boats.json", boatsJson, Encoding.UTF8);
        }
    }
}
